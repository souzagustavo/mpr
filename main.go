package main

import (
	"mpr/cg"
	"os"
)

func main() {
	dir, _ := os.Getwd()
	root := dir + "\\resources\\ARTERIELLE_6168"

	decoder := cg.NewDecoder(cg.RepositoryConfig{
		Root: root, Width:  512, Height: 512,
	})

	decoder.Cut(320, 300, 150)
}
