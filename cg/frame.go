package cg

import (
	"image"
	"image/color"
)

type Frame [][]uint16

func NewFrameEmpty() Frame {
	return Frame{}
}

func NewFrame(x, y int) Frame {
	frame := make([][]uint16, x)
	for index,_ := range frame {
		frame[index] = make([]uint16, y)
	}
	return frame
}

func (f Frame) ToImg() *image.Gray {
	xmax := len(f)
	ymax := len(f[0])
	img := image.NewGray(image.Rect(0,0,xmax,ymax))
	for x := 0; x < xmax; x++ {
		for y := 0; y < ymax; y++ {
			img.Set(x, y, color.Gray{Y: uint8(f[x][y])})
		}
	}
	return img
}



func (f Frame) Pivot() Frame {
	xMax := len(f)
	yMax := len(f[0])

	frame := NewFrame(yMax, xMax)

	for x := 0; x < xMax; x++ {
		for y := 0; y < yMax; y++ {
			frame[y][x] = f[x][y]
		}
	}
	return frame
}

func (f Frame) Copy() Frame {
	frame := NewFrameEmpty()
	for _, vet := range f {
		frame = append(frame, vet)
	}
	return frame
}
