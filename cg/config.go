package cg

type RepositoryConfig struct {
	Root string
	Width, Height int
}
