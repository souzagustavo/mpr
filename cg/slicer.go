package cg



type Slicer struct {
	zNow int
	normal           Frame
	sagital          Frame
	coronal          Frame
}

func NewSlicer(width, height int) *Slicer {
	return &Slicer{
		normal: NewFrame(width, height),
		sagital: NewFrameEmpty(),
		coronal: NewFrameEmpty(),
	}
}

func (s *Slicer) calculate(x,y,z int, frame Frame){
	var sagital = s.getSagitalSlice(y, frame)
	var coronal = s.getCoronalSlice(x, frame)
	if s.zNow == z {
		s.normal = frame.Copy()
	}
	s.sagital = append(s.sagital, sagital)
	s.coronal = append(s.coronal, coronal)
}

func (s *Slicer) setCut(x,y int,frame Frame){
	for i := 0; i < len((frame)[x]); i ++{
		(frame)[x][i] = 255
	}
	for i := 0 ; i < len(frame); i++ {
		(frame)[i][y] = 255
	}
}

func (s *Slicer) getSagitalSlice(y int, frame Frame) []uint16 {
	var sagital []uint16
	for x := 0 ; x < len(frame); x++{
		sagital = append(sagital, frame[x][y])
	}
	return sagital
}

func (s *Slicer) getCoronalSlice(x int, frame Frame) []uint16 {
	return append([]uint16{}, frame[x]...)
}


func (s *Slicer) pivot(x,y,z int){
	var pivot Frame

	s.setCut(z,x, s.coronal)
	s.setCut(x,y, s.normal)
	s.setCut(z,y, s.sagital)

	pivot = s.sagital.Pivot()
	s.sagital = pivot

	pivot = s.coronal.Pivot()
	s.coronal = pivot
}