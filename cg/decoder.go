package cg

import (
	"fmt"
	"image"
	"image/color"
	"image/jpeg"
	"io/ioutil"
	"log"
	"os"
	"sort"
)

type Decoder struct {
	Rep RepositoryConfig
}

func NewDecoder(rep RepositoryConfig) *Decoder {
	return &Decoder{rep}
}

func (d Decoder) Cut(x, y, z int) {
	slicer := NewSlicer(d.Rep.Width, d.Rep.Height)

	files := d.GetSortFiles()

	var frame Frame
	for oz, f := range files {
		slicer.zNow = oz

		frame = NewFrame(d.Rep.Width, d.Rep.Height)
		d.getFileFrame(f, &frame)
		slicer.calculate(x, y, z, frame)
	}
	slicer.pivot(x,y,z)

	d.encodeToJpg(slicer)
}

func (d Decoder) getFileFrame(filename string, frame *Frame) {
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	jpg, err := jpeg.Decode(f)
	if err != nil {
		log.Fatal(err)
	}
	var r,g,b,_ uint32
	for  x := 0; x < d.Rep.Height; x++ {
		for y := 0; y < d.Rep.Width; y++ {
			r,g,b,_ = jpg.At(x,y).RGBA()
			g := (0.299*float64(r) + 0.587*float64(g) + 0.114*float64(b)) / 256
			(*frame)[x][y] = uint16(color.Gray{Y: uint8(g)}.Y)
		}
	}
}

//codifica fatias mpr para png. Imagens salvas em /resources
func (d Decoder) encodeToJpg(slicer *Slicer){
	var img *image.Gray

	img = slicer.normal.ToImg()
	toJpg(img, "transversal.jpg")

	img = slicer.coronal.ToImg()
	toJpg(img, "coronal.jpg")

	img = slicer.sagital.ToImg()
	toJpg(img, "sagital.jpg")
}

func toJpg(img image.Image, filename string) {
	toImg, _ := os.Create("resources/"+filename)
	defer toImg.Close()
	err := jpeg.Encode(toImg, img, nil)
	if err != nil {
		_= fmt.Errorf("%s", err.Error())
	}
}

func (d Decoder)  GetSortFiles() []string {
	fileInfo, err := ioutil.ReadDir(d.Rep.Root)
	if err != nil {
		log.Fatal(err)
	}
	var sorted []string
	for _, file := range fileInfo {
		sorted = append(sorted, d.Rep.Root + "\\"+file.Name())
	}
	sort.Strings(sorted)
	return sorted
}


